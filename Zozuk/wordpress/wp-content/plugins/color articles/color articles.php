<?php

/*
Plugin Name: Color Articles
Description: color a, an and the from all posts
Version: 1.0
Author: RusPotd
*/

function wpblog_modify_content( $content ) {
	
	$post = new DOMDocument();
    $post->loadHTML( $content );

	$paragraphs = $post->getElementsByTagName( 'p' );
	
	foreach($paragraphs as $para){
		
		$line = $para->nodeValue;
		$delimiter = ' ';
		$words = explode($delimiter, $line);
		 
		foreach ($words as $word) {
			if($word == "a" OR $word == "an" OR $word == "the"){
				$line.="<strong style='color: red;'> $word</strong>";
			}
			else{
				$line.=" ".$word;
			}
		}

		$para->nodeValue = "<p>".$line."</p>";
		echo $para->nodeValue;
		
	}
	
    return '';//$duplicate;
}

add_filter( 'the_content', 'wpblog_modify_content' );

?>