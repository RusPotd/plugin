<?php
/*
Plugin Name: Books
Description: Create posts of Book category
Version: 1.0
Author: RusPotd
*/

register_activation_hook( __FILE__, 'books_activate' );
register_deactivation_hook( __FILE__, 'books_deactivate' );

function books_activate(){
	//activated
}

function books_deactivate(){
	//deactivated
}

/* Create custome texanomy */

function taxonomies_fun(){
	
	$labels = array(
		'name'          => __('Genre'),
        'singular_name' => __('Genre'),
		'capability_type' => 'post'
	);
	
	$args = array(
		'labels' => $labels,
		'heirarchical' => true,
		"show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
		'show_admin_column' => true,
		'query_var' =>  true,
		'rewrite' => array( 'slug' => 'genre' ),
	);
	
	register_taxonomy('genre', 'wporg_product', $args);
	
}

add_action('init', 'taxonomies_fun');

/* Create custome post type Book */

function books_post_type() {
    register_post_type('wporg_product',
        array(
				'labels'      => array(
                'name'          => __('Books'),
                'singular_name' => __('Books'),
				'capability_type' => 'post',
            ),
                'public'      => true,
                'has_archive' => true,
				'rewrite'     => array( 'slug' => 'books' ),
        )
    );
}

add_action('init', 'books_post_type');


/* Custom Meta Boxes */

function books_add_meta_box(){
	add_meta_box('author_name', 'Book Information', 'boooks_author_name_callback', 'wporg_product');
} 

function boooks_author_name_callback( $post){
	wp_nonce_field('books_save_data', 'books_meta_box_nonce');
	
	$author_name = get_post_meta($post->ID, '_author_name_key', true);
	$isbn = get_post_meta($post->ID, '_isbn_key', true);
	$start_date = get_post_meta($post->ID, '_start_date_key', true);
	$finish_date = get_post_meta($post->ID, '_finish_date_key', true);
	
	echo '<table><tr><td><label for="books_author_field"> Author Name</label></td>';
	echo '<td><input type="text" id="books_author_field" name="books_author_field" value="'.esc_attr($author_name).'" required /></td></tr>';
	
	echo '<tr><td><label for="books_isbn_field"> ISBN</label></td>';
	echo '<td><input type="text" id="books_isbn_field" name="books_isbn_field" value="'.esc_attr($isbn).'" required /></td></tr>';
	
	echo '<tr><td><label for="books_read_start_field"> Started Reading Date</label></td>';
	echo '<td><input type="text" id="books_read_start_field" name="books_read_start_field" value="'.esc_attr($start_date).'" required /></td></tr>';
	
	echo '<tr><td><label for="books_finish_read_field"> Finished Reading Date</label></td>';
	echo '<td><input type="text" id="books_finish_read_field" name="books_finish_read_field" value="'.esc_attr($finish_date).'" /></td></tr></table>';
}

function books_meta_data_save( $post_id){
	
	if(! current_user_can('edit_post', $post_id)){
		return;
	}
	
	$author = sanitize_text_field($_POST['books_author_field']);
	$isbn = sanitize_text_field($_POST['books_isbn_field']);
	$start = sanitize_text_field($_POST['books_read_start_field']);
	$end = sanitize_text_field($_POST['books_finish_read_field']);
	
	update_post_meta($post_id, '_author_name_key', $author);
	update_post_meta($post_id, '_isbn_key', $isbn);
	update_post_meta($post_id, '_start_date_key', $start);
	update_post_meta($post_id, '_finish_date_key', $end);
}

add_action('admin_init', 'books_add_meta_box');
add_action('save_post', 'books_meta_data_save');

?>