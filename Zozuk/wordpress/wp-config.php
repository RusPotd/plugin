<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'zozuk' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ')#64 r=D9|@|e*%mUE11n30qYN-@nom7HuOuK yl!XS^HLR@x3Q&|C!w%VIouDM?' );
define( 'SECURE_AUTH_KEY',  '71=HuG}Odhc8{@D+4NuW;ZR6|=1-U!/?`!Zd@@.*VloAu#_Pc=arI}r]rrEMm/kB' );
define( 'LOGGED_IN_KEY',    '3z)PO3B;{ons$$/;&/k@3kEn/9:aIpo5T]>Rh7s)C]|$AQV19Fbmj/s*M&NWKvb@' );
define( 'NONCE_KEY',        'Iehzze0MoS2<4Jm9W<)x!vBbHgd=I-Ey#i7H~CC@?rw.(})y6,`oG:u_<=ix{uO!' );
define( 'AUTH_SALT',        'XcB2+ozv4oEGo!Q9D0s$w,mVfnpwY/Ty,@yU-KG7~rg%.%E_>NX2sp)9 [oM9Llw' );
define( 'SECURE_AUTH_SALT', '6<xiUHyYO|v3,mw]tfh}&]/Zf)!RH)/(9GhnW[Y9LI x,WOJ2b8)Nr],DD}8u-!g' );
define( 'LOGGED_IN_SALT',   '? u>5bj#~<mdmn*pcs%R*=h;o0$g/$26](_q0D2R%c%IbGXQm^[Lv6k@.TlenW5j' );
define( 'NONCE_SALT',       '>;}GGo{Mk?+FYM}U`=b<|rLB6l9B)>L/{%eiO^EqSW@ELpj$3-)jU6he/$,$ZG? ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
